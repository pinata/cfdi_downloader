defmodule CfdiDownloader.Search do
  import CfdiDownloader
  @user_agent "#{CfdiDownloader.user_agent}"
  @sat_download_received "https://portalcfdi.facturaelectronica.sat.gob.mx/ConsultaReceptor.aspx"
  @max_timeout 120000
  @cfdi_status %{ :all => -1, :cancelled => 0, :active => 1}

  def execute(ciec, date, status \\ :all) do
    case CfdiDownloader.Login.execute(ciec) do
      {:error, message } ->
        {:error, message}
      {session, _ } ->
        search_with_date(session, date, status)
    end
  end

  defp search_with_date(session, date, status) do
    first_form = HTTPotion.get(@sat_download_received,
      [ headers: ["User-Agent": @user_agent, "Cookie": session],
        timeout: @max_timeout])
      |> set_search_form(date, status)

    search_form = HTTPotion.post(@sat_download_received,
      [ body: encode_body(first_form),
        headers: ["Content-type": "application/x-www-form-urlencoded",
        "User-Agent": @user_agent,
        "Cookie": session],
        timeout: @max_timeout])
      |> set_search_form(date, status)

    cfdis = HTTPotion.post(@sat_download_received,
      [ body: encode_body(search_form),
        headers: ["Content-type": "application/x-www-form-urlencoded",
        "User-Agent": @user_agent,
        "Cookie": session],
        timeout: @max_timeout])
      |> cfdi_list
    { cfdis, session }
  end

  defp cfdi_list(response) do
    response.body
    |> Floki.find("#DivPaginas div table tr")
    |> Enum.map(fn(tr) ->
      tr
      |> cfdi_format
    end)
  end

  defp cfdi_format({ _, _,
    [download, uuid, rfc_e, name_e, rfc_r, name_r, emission, certification,
    pac, total, effect, state, cancellation ]}) do
      %CfdiDownloader.Cfdi{
        uuid: Floki.text(uuid), sender_rfc: Floki.text(rfc_e),
        sender_name: Floki.text(name_e), receiver_rfc: Floki.text(rfc_r),
        receiver_name: Floki.text(name_r), emission_date: Floki.text(emission),
        certification_date: Floki.text(certification), pac: Floki.text(pac),
        effect: Floki.text(effect), cancellation_date: Floki.text(cancellation),
        state: Floki.text(state), download_url: download_url(download),
        total: Floki.text(total)}
  end

  defp all_inputs(response) do
    response.body
    |> Floki.find("input[value]")
    |> Enum.reject(fn(i) ->
      name = i
      |> Floki.attribute("name")
      |> List.first
      String.contains?(name, ["ctl00$MainContent$BtnDescargar", "ctl00$MainContent$BtnImprimir"])
    end)
    |> Enum.reduce(%{}, fn(i, acc) ->
      value = i
        |> Floki.attribute("value")
        |> List.first

      key = i
      |> Floki.attribute("name")
      |> List.first

      Map.put(acc, key, value)
    end)
  end

  defp set_search_form(response, date, status) do
    response
    |> all_inputs
    |> Map.put("ctl00$MainContent$CldFecha$DdlAnio", date.year)
    |> Map.put("ctl00$MainContent$CldFecha$DdlMes", date.month)
    |> Map.put("ctl00$MainContent$CldFecha$DdlDia", date_left_pad(date.day))
    |> Map.put("ctl00$MainContent$CldFecha$DdlHora", 0)
    |> Map.put("ctl00$MainContent$CldFecha$DdlMinuto", 0)
    |> Map.put("ctl00$MainContent$CldFecha$DdlSegundo", 0)
    |> Map.put("ctl00$MainContent$CldFecha$DdlHoraFin", 23)
    |> Map.put("ctl00$MainContent$CldFecha$DdlMinutoFin", 59)
    |> Map.put("ctl00$MainContent$CldFecha$DdlSegundoFin", 59)
    |> Map.put("ctl00$MainContent$TxtRfcReceptor", "")
    |> Map.put("ctl00$MainContent$DdlEstadoComprobante", @cfdi_status[status])
    |> Map.put("ctl00$MainContent$hfInicialBool", false)
    |> Map.put("ctl00$MainContent$hfDescarga", "")
    |> Map.put("ctl00$MainContent$ddlComplementos", -1)
    |> Map.put("ctl00$MainContent$BtnBusqueda", "Buscar CFDI")
    |> Map.to_list
  end

  defp download_url(td) do
    case Floki.find(td, "#BtnDescarga") do
      [] ->
        nil
      button ->
        button
        |> Floki.attribute("onclick")
        |> List.first
        |> String.split(",")
        |> List.first
        |> String.split("(")
        |> List.last
        |> String.replace("'","")
    end
  end

  defp date_left_pad(num) do
    num
    |> Integer.to_string
    |> String.rjust(2, ?0)
  end
end
