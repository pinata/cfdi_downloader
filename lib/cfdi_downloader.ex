defmodule CfdiDownloader do
  @sat_download_index "https://portalcfdi.facturaelectronica.sat.gob.mx/"
  @user_agent "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:53.0) Gecko/20100101 Firefox/53.0"

  def sat_download_index do
    @sat_download_index
  end

  def user_agent do
    @user_agent
  end

  def encode_body(form) do
    Enum.map(form, fn({key, value}) ->
      "#{key}=#{URI.encode_www_form(to_string(value))}"
    end)
    |> Enum.join("&")
  end

  def cookies(cookie_jar, response) do
    case response.headers["set-cookie"] do
      nil ->
        cookie_jar
      headers when is_list(headers) ->
        headers
        |> Enum.reduce(cookie_jar, fn(value, acc) ->
          [k,v] = split_cookie(value)
          Map.put(acc, k, v)
        end)
      value ->
        [k,v] = split_cookie(value)
        Map.put(cookie_jar, k, v)
    end
  end

  def format_cookies(cookie_jar) do
    cookie_jar
    |> Enum.map(fn({k, v}) ->
      "#{k}=#{v}"
    end)
    |> Enum.join("; ")
  end

  def split_cookie(value) do
    new_value = value |> String.split(";") |> List.first
    [key | values] =  String.split(new_value, "=")
    [key, Enum.join(values, "=")]
  end
end
