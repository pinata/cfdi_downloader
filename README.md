# CfdiDownloader

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add cfdi_downloader to your list of dependencies in `mix.exs`:

        def deps do
          [{:cfdi_downloader, "~> 0.0.1"}]
        end

  2. Ensure cfdi_downloader is started before your application:

        def application do
          [applications: [:cfdi_downloader]]
        end

