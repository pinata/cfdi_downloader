defmodule CfdiDownloader.Cfdi do
  defstruct [:download_url, :uuid, :sender_rfc, :sender_name, :receiver_rfc,
    :receiver_name, :emission_date, :certification_date, :pac, :effect,
    :state, :cancellation_date, :total]
end
