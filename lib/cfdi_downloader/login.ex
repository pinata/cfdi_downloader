defmodule CfdiDownloader.Login do
  import CfdiDownloader

  @sat_login_url "https://cfdiau.sat.gob.mx/nidp/app/login?id=mat-ptsc-cont&sid=2&option=credential"
  @sat_download_index "#{CfdiDownloader.sat_download_index}"
  @sat_download_federation_url "https://cfdicontribuyentes.accesscontrol.windows.net/v2/wsfederation"
  @max_timeout 120000
  @user_agent "#{CfdiDownloader.user_agent}"

  def execute(ciec) do
    login_post = HTTPotion.post @sat_login_url,
      [body: "Ecom_User_ID=#{ciec.rfc}&Ecom_Password=#{ciec.password}&option=credential&submit=Enviar",
      headers: ["User-Agent": @user_agent, "Content-Type": "application/x-www-form-urlencoded"],
      timeout: @max_timeout]

    case String.contains?(login_post.body, "incorrectos") do
      true ->
        { :error, "invalid login"}
      false ->
        session = %{} |> cookies(login_post)
        login_download_url(session, 0)
      end
  end

  def login_download_url(login_cookies, tries) do
    first_response = HTTPotion.get @sat_download_index,
        [ headers: ["User-Agent": @user_agent,],
          timeout: @max_timeout]

    auth_url  = first_response.headers["location"]
    auth_response = HTTPotion.get auth_url,
      [ headers: ["User-Agent": @user_agent,],
          timeout: @max_timeout]

    login_auth_url = auth_response.headers["location"]
    download_response = HTTPotion.get login_auth_url,
      [ headers: ["User-Agent": @user_agent,
        "Cookie": format_cookies(login_cookies)],
        timeout: @max_timeout]


    [wa1, wresult1, wctx1] = download_response |> hidden_inputs
    federation_response = HTTPotion.post @sat_download_federation_url,
      [ body: encode_body([wa: wa1, wresult: wresult1, wctx: wctx1]),
        headers: ["User-Agent": @user_agent,
        "Content-Type": "application/x-www-form-urlencoded"],
        timeout: @max_timeout]

    [wa2, wresult2, wctx2] = federation_response |> hidden_inputs
    download_auth_response = HTTPotion.post @sat_download_index,
      [ body: encode_body([wa: wa2, wresult: wresult2, wctx: wctx2]),
        headers: ["User-Agent": @user_agent,
        "Content-Type": "application/x-www-form-urlencoded"],
        timeout: @max_timeout]

    fed_auth_cookies = cookies(%{}, download_auth_response)

    response = HTTPotion.get @sat_download_index, [ headers: ["User-Agent": @user_agent,
            "Cookie": format_cookies(fed_auth_cookies)],
            timeout: @max_timeout]

    session = cookies(%{}, response)

    case String.contains?(response.body, "Consultar Facturas Recibidas") do
      true ->
        { format_cookies(session) , response }
      false ->
        if tries <= 0 do
          {:error, "Too many tries"}
        else
          login_download_url(login_cookies, tries - 1)
        end
    end
  end

  defp hidden_inputs(response) do
    response.body
    |> Floki.find("input[type='hidden']")
    |> Enum.map(fn(i) ->
      i
      |> Floki.attribute("value")
      |> List.first
    end)
  end
end