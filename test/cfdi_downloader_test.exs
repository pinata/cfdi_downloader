defmodule CfdiDownloaderTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock

  doctest CfdiDownloader

  setup_all do
    ExVCR.Config.cassette_library_dir("fixture/vcr_cassettes")
    :ok
  end

  test "should store RFC and password information" do
    ciec = %CfdiDownloader.Ciec{rfc: "XEXX010101000", password: "12345678"}
    assert ciec.rfc == "XEXX010101000"
    assert ciec.password == "12345678"
  end

  test "should identity wrong login" do
    use_cassette "fail_login" do
      ciec = %CfdiDownloader.Ciec{rfc: "HELLO", password: "password"}
      {status, _ } = CfdiDownloader.Login.execute(ciec)
      assert status == :error
    end
  end

  test "should parse cookies correctly" do
    cookie = "FedAuthCONT1=dVZucGRhd2tjNTFwYitoN1hqb0hkK01KR2hCVUpZTzltcTdRejhqRUtaZW42cUlCaG1hTzY1aWVHQURLaDIrS2tMaVZxQ3YvdXhRYmV5akxRWEhrZGI2aXlTSnc0QVZoVjhhZ0pEUDJSMnJQK1kxN0h0d2dEblpCWG85b0NLcEhEQ0dOVXFYZG9DaWpkMS9JYkZSK0lyeDRuL2pVUFZORTB2Z201cFd2SE5ZVk1QMjgwRDNGY3B4d1I1WVFxVlJXY3Q2QWtqNlU4cHdkZW5kMEEzdDhzRnY5ZTg0RXU1bmtPcDd6NHZIaUJBdGUydlBMSnVmSnAyTWFUdGd0L2xabDZNdDBqWGRsQkYvYnU2ekZ4WVh5V0tyST08L0Nvb2tpZT48L1NlY3VyaXR5Q29udGV4dFRva2VuPg==; path=/; HttpOnly"
    [k,v] = CfdiDownloader.split_cookie(cookie)
    assert k == "FedAuthCONT1"
    assert v == "dVZucGRhd2tjNTFwYitoN1hqb0hkK01KR2hCVUpZTzltcTdRejhqRUtaZW42cUlCaG1hTzY1aWVHQURLaDIrS2tMaVZxQ3YvdXhRYmV5akxRWEhrZGI2aXlTSnc0QVZoVjhhZ0pEUDJSMnJQK1kxN0h0d2dEblpCWG85b0NLcEhEQ0dOVXFYZG9DaWpkMS9JYkZSK0lyeDRuL2pVUFZORTB2Z201cFd2SE5ZVk1QMjgwRDNGY3B4d1I1WVFxVlJXY3Q2QWtqNlU4cHdkZW5kMEEzdDhzRnY5ZTg0RXU1bmtPcDd6NHZIaUJBdGUydlBMSnVmSnAyTWFUdGd0L2xabDZNdDBqWGRsQkYvYnU2ekZ4WVh5V0tyST08L0Nvb2tpZT48L1NlY3VyaXR5Q29udGV4dFRva2VuPg=="
  end

  # Passes but VCR cannot save it
  # test "login correctly with a valid" do
  #  use_cassette "login" do
  #     ciec = %CfdiDownloader.Ciec{rfc: "GOER860507DY3", password: "password"}
  #     {session, _ } = CfdiDownloader.Login.execute(ciec)
  #     assert session != :error
  #   end
  # end

  # test "login and get UUID of recieved CFDIs" do
  #   use_cassette "download" do
  #     ciec =  %CfdiDownloader.Ciec{rfc: "GOER860507DY3", password: "password"}
  #     date =  %Date{year: 2017, month: 1, day: 3}
  #     {[cfdi|_], session } = CfdiDownloader.Search.execute(ciec, date)
  #     assert String.contains?(cfdi.uuid, "BB851AB8-E034-4959-9AE2-CD479BA92ED4")
  #     {:ok, body } = CfdiDownloader.Download.execute(cfdi, session)
  #     xml = body |> String.slice(3,3)
  #     assert xml == "xml"
  #   end
  # end

  test "handle cfdi without a url" do
    cfdi = %CfdiDownloader.Cfdi{download_url: nil}
    {:error, _ } = CfdiDownloader.Download.execute(cfdi, nil)
  end
end
