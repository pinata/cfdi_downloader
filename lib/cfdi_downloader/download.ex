defmodule CfdiDownloader.Download do

  @sat_download_index "#{CfdiDownloader.sat_download_index}"

  def execute(cfdi, session) do
    case cfdi.download_url do
      nil ->
        {:error, ""}
      download_url ->
        {:ok, HTTPotion.get("#{@sat_download_index}/#{download_url}",
            [headers: ["Cookie": session]]).body }
    end
  end
end
